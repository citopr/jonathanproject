//
//  AppDelegate.h
//  TestProject
//
//  Created by Abraham Ventura on 6/12/12.
//  Copyright (c) 2012 Prolific Methods LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end

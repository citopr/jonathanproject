//
//  ViewController.m
//  TestProject
//
//  Created by Abraham Ventura on 6/12/12.
//  Copyright (c) 2012 Prolific Methods LLC. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize length;
@synthesize diameterLabel;
@synthesize totalWeight;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [self setLength:nil];
    [self setDiameterLabel:nil];
    [self setTotalWeight:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}


- (IBAction)chooseTreeButton:(id)sender {
    
    
}

- (IBAction)Calculate:(id)sender {
    
    float diameter = self.diameterLabel.text.floatValue;
    float preWeightInPounds = powf((diameter/2)/12, 2) * M_PI;
    float weightInPounds = preWeightInPounds * self.length.text.floatValue;
    float weightTotal = weightInPounds * 55;
    self.totalWeight.text = [NSString stringWithFormat:@"%.2f", weightTotal];
    
}
@end
